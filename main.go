// A crawler that collects BGP updates dump information for first and last
// available data for all collectors in RIPE RRC and RouteViews
package main

import (
	"gitlab.com/mingwei/go-bgptracker/trackers"
	"gitlab.com/mingwei/go-bgptracker/web"
	"fmt"
	"time"
)

// a map of collector name to collector information
var collectorsMap = make(map[string]trackers.CollectorInfo)

// a global variable that stores the last-crawling timestamp
var lastCrawlTime time.Time

func main() {
	// start the web service at the beginning, run as a go routine
	go web.StartServer(collectorsMap, &lastCrawlTime)

	// start crawling before the ticker begins
	crawl()

	// create a ticker that runs for every two minutes
	ticker := time.NewTicker(time.Minute * 2)
	for range ticker.C {
		// when ticker triggers a timeout, crawl the collectors
		crawl()
	}
}

func crawl() {
	// update the last-crawling time
	fmt.Println("update update files at time", time.Now())
	lastCrawlTime = time.Now()

	// crawl RouteViews collectors
	fmt.Println("\t RouteViews")
	rv := trackers.RvSource{RootUrl: "http://archive.routeviews.org/"}
	rvCollectors := rv.GetCollectorInfo()
	for _, v := range rvCollectors {
		// update map for web display
		collectorsMap[v.CollectorName] = v
	}

	// crawl RIPE RRC collectors
	fmt.Println("\t RIPE")
	ripe := trackers.RipeSource{RootUrl: "http://data.ris.ripe.net/"}
	ripeCollectors := ripe.GetCollectorInfo()
	for _, v := range ripeCollectors {
		// update map for web display
		collectorsMap[v.CollectorName] = v
	}

	fmt.Println("\t Done")
}
