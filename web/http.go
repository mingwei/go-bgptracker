package web

import (
	"gitlab.com/mingwei/go-bgptracker/trackers"
	"fmt"
	"log"
	"net/http"
	"sort"
	"time"
)

func StartServer(collectorsMap map[string]trackers.CollectorInfo, timePtr *time.Time) {

	h := http.NewServeMux()

	h.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		var collectors []string
		for k := range collectorsMap {
			collectors = append(collectors, k)
		}
		sort.Strings(collectors)

		fmt.Fprintln(w, "<!DOCTYPE html>")
		fmt.Fprintln(w, "<html>")
		fmt.Fprintln(w, "<head>\r<style>\rtable, th, td {\rborder: 1px solid black;\r}\r</style>\r</head>")
		fmt.Fprintln(w, "<body>")

		var tmpl = "<tr>" +
			"<td><a href=\"%s\">%s</a></td>" +
			"<td>%s</td><td>%s</td></tr>\r"
		fmt.Fprintln(w, "<h1>BGP Data Sources First and Last Dump File Time</h1>")
		fmt.Fprintln(w, "<table>")
		fmt.Fprintln(w, "<tr><td>Name</td><td>First</td><td>Last</td></tr>")
		for _, v := range collectors {
			fmt.Fprintf(w, tmpl,
				collectorsMap[v].BaseUrl, collectorsMap[v].CollectorName,
				collectorsMap[v].FirstUrl, collectorsMap[v].LastUrl)
		}
		fmt.Fprintln(w, "</table>")
		fmt.Fprintln(w, "Last updated:", timePtr.String())

		fmt.Fprintln(w, "</body>\r</html>")
	})

	err := http.ListenAndServe(":9999", h)
	log.Fatal(err)
}
